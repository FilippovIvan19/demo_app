package com.example.restservice;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @GetMapping("/greeting")
    public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }

    @GetMapping("/getMode")
    public long getMode(@RequestParam(value = "number") long number, @RequestParam(value = "mode") long mode) {
        return number % mode;
    }

    @PostMapping("/modifyString")
    public String modifyString(@RequestParam(value = "userName") String userName, @RequestBody MessageForm msg) {
        return msg.macrostr.replace("##user_name##", userName);
    }

    // GET: /getMode. params: number, mode. return number - метод для получения значения number по модулю mode. (использовать BigInteger)

    // POST: /modifyString. params: userName, body: message, return: String - метод для подставления в строку имени юзера.
    // Т.е. в параметрах отправляем имя пользователя, а в бади отправляем строку, внутри которой есть макрос (например ##user_name##), который нужно заменить на имя пользователя.
}
